# Structure-and-Interpretation-of-Classical-Mechanics

## MIT
* [*Structure and Interpretation of Classical Mechanics*
  ](https://mitpress.mit.edu/sites/default/files/titles/content/sicm_edition_2/toc.html)
* [*Structure and Interpretation of Classical Mechanics*
  ](https://books.google.ca/books?id=a4-pBgAAQBAJ&pg=PR16&lpg=PR16&dq=scmutils&source=bl&ots=Zfp01xVLa4&sig=ACfU3U3Pg61aFsm9GpiXp9e71iHZ6FUsDA&hl=fr&sa=X&ved=2ahUKEwiJ2rWY6KDjAhUu1VkKHdidBpU4HhDoATANegQICRAB#v=onepage&q=scmutils&f=false)
* [Scheme Mechanics Installation for GNU/Linux or Mac OS X
  ](https://ocw.mit.edu/courses/earth-atmospheric-and-planetary-sciences/12-620j-classical-mechanics-a-computational-approach-fall-2008/tools/)
  (Contains Scmutils)

## Wikipedia
* [*Structure and Interpretation of Classical Mechanics*
  ](https://en.m.wikipedia.org/wiki/Structure_and_Interpretation_of_Classical_Mechanics)

## WorldCat
* [Structure and Interpretation of Classical Mechanics](https://www.worldcat.org/title/structure-and-interpretation-of-classical-mechanics/oclc/903531656/editions)

## See Also
* [Steeb, W.-H. (2010). Quantum Mechanics Using Computer Algebra, second edition,
  World Scientific Publishing, Singapore.
  ](https://google.com/search?q=Steeb%2C+W.-H.+%282010%29.+Quantum+Mechanics+Using+Computer+Algebra%2C+second+edition%2C+World+Scientific+Publishing%2C+Singapore)